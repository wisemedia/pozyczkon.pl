<main role="main">
	<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div class="section-title"><?php the_title() ?></div>
		<div class="container registration">
			<?php if( $status_code == 202 ) : ?>
				<?php if( $response->status == 'rejected' ) : ?>
					<!-- rejected -->
					<p><strong>Gracias por completar el formulario.</strong></p> 
					<p>Sentimos comunicarte que tu solicitud no cumple nuestros requisitos y ha sido rechazada.</p>
				<?php elseif( $response->status == 'under_investigation' ) : ?>
					<!-- under investigation -->
					<p><strong>Gracias.</strong></p>
					<p>Tu solicitud ha sido enviada correctamente.</p>
					<p>Te informaremos de la resolución por correo electrónico.</p>
				<?php endif; ?>
			<?php elseif( $status_code == 400 ) : ?>
				<!-- missing parameters -->
				<p><strong>Gracias por completar la solicitud.</strong></p>
				<p>Alguno de los campos fueron rellenados de forma incorrecta.</p>
				<p>Por favor, vuelve a intentarlo.</p>
			<?php elseif( $status_code == 401 || $status_code == 403 ) : ?>
				<!-- unauthorized -->
				<p><strong>Gracias por completar la solicitud.</strong></p>
				<p>Lamentablemente hemos tenido un error técnico.</p>
				<p>Por favor, vuélvelo a intentar más tarde.</p>
			<?php endif; ?>

		</div>
		<?php
	endwhile;
	endif;
	?>
</main>