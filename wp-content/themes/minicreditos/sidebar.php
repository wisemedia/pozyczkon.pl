<aside class="sidebar" role="complementary">
	<?php if (is_active_sidebar('minicreditos-sidebar')) : ?>
		<div id="widget-area" class="widget-area" role="complementary">
			<?php dynamic_sidebar('minicreditos-sidebar'); ?>
		</div>
	<?php endif; ?>
</aside>