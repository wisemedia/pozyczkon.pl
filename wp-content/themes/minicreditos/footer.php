<footer itemscope itemtype="https://schema.org/WPFooter">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div class="logo">
					<a href="<?php echo get_home_url() ?>">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png" alt="minicréditos enlínea"/>
					</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8">
				<nav>
					<?php
					wp_nav_menu(array(
						'menu' => 'footer',
						'menu_class' => 'clearfix',
						'depth' => 1,
						'theme_location' => 'footer'
					))
					?>
				</nav>
			</div>
		</div>
	</div>
	<?php if(!$_COOKIE['minutovapozicka_cookie_accept']) : ?>
	<div class="box-cookies-wrapper">
		<div class="container">
			<p>Vážení klienti, radi by sme Vás upozornili, že za účelom zaistenia čo najvyššej kvality našich služieb, používajú naše stránky súbory cookies". Zhromažďovanie informácií a prístupov "cookies" je možné nastaviť pomocou webového prehliadača. Používaním tohoto webu súhlasíte s použitím súborov "cookies". Ďalšie informácie o súboroch "cookies" nájdete <a href="https://www.google.com/policies/technologies/cookies/" target="_blank">tu</a>.</p>
			<a href="#" class="button red">Súhlasím</a>
		</div>
	</div>
	<?php endif; ?>
</footer>    </div>
<?php
wp_footer();
?>
</body></html>