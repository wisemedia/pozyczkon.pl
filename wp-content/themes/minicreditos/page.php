<?php get_header(); ?>
<main role="main">
	<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div class="section-title"><?php the_title() ?></div>
		<div class="container">
			<?php the_content(); ?>
		</div>
		<?php
	endwhile;
	endif;
	?>
</main>
<?php get_footer(); ?>
