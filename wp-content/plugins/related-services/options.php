<?php

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

add_action('admin_menu', 'related_services_add_settings_page');
add_action('admin_init', 'related_services_register_settings_fields');

function related_services_add_settings_page() {
	add_options_page(
		'Settings Admin',
		'External Link Obfuscator',
		'manage_options',
		'related-services',
		'related_services_admin_page'
	);
}

function related_services_admin_page() { ?>
	<div class="wrap">
		<h2>Servicios relacionados</h2>
		<form method="post" action="options.php">
			<?php
			settings_fields('related_services_general');
			do_settings_sections('related_services_settings_general_page');
			submit_button();
			?>
		</form>
	</div>
	<?php
}

function related_services_get_defaults() {
	return array();
}

function related_services_get_options() {
	static $options;
	if (!isset($options)) {
		$options = wp_parse_args(get_option('related_services_general_options'), related_services_get_defaults());
	}

	return $options;
}

function related_services_settings_general_section_header() {
}

function related_services_register_settings_fields() {
	register_setting(
		'related_services_general', // Option group
		'related_services_general_options' // Option name
	);

	add_settings_section(
		'related_services_settings_general_section', // ID
		__('Opciones generales'), // Title
		'related_services_settings_general_section_header', // Callback
		'related_services_settings_general_page' // Page
	);
	/*
		// URL redirección
		add_settings_field(
			'goto_script_file', // ID
			__('Redirection file'), // Title
			array($this, 'gotoScriptField'), // Callback
			'related_services_settings_general_page', // Page
			'related_services_settings_general_section' // Section
		);
	*/

	add_settings_field(
		"related-services-services", // ID
		'Servicios activos',
		'related_services_service_checkbox_fields',
		'related_services_settings_general_page', // Page
		'related_services_settings_general_section' // Section
	);
	add_settings_field(
		"related-services-alt", // ID
		'Servicio alternativo',
		'related_services_alt_service_field',
		'related_services_settings_general_page', // Page
		'related_services_settings_general_section' // Section
	);
}

function related_services_service_checkbox_fields() {
	$options = related_services_get_options();
	$services = get_posts(array(
		'post_type' => 'servicio',
		'post_status' => 'publish',
		'numberposts' => -1
	));
	foreach ($services as $service) {
		printf('<p><label><input type="checkbox"%s value="%d" name="related_services_general_options[services][]" /> %s</label></p>',
			in_array($service->ID, $options['services']) ? ' checked="checked"' : '',
			$service->ID,
			$service->post_title
		);
	}
}

function related_services_alt_service_field() {
	$options = related_services_get_options();
	$services = get_posts(array(
		'post_type' => 'servicio',
		'post_status' => 'publish',
		'numberposts' => -1
	));
	echo '<select name="related_services_general_options[alt-service]"><option value="">- Selecciona -</option>';
	foreach ($services as $service) {
		printf('<option%s value="%d">%s</option>',
			$service->ID == $options['alt-service'] ? ' selected="selected"' : '',
			$service->ID,
			$service->post_title
		);
	}
	echo '</select>';
}

/*

/*
	// Hashing seed
	add_settings_field(
		'hashing_seed', // ID
		__('Hashing seed'), // Title
		array( $this, 'hashingSeedField' ), // Callback
		'related_services_settings_general_page', // Page
		'related_services_settings_general_section' // Section
	);

	// tinymce button
	add_settings_field(
		'tinymce_button',
		__('Add editor button'),
		array( $this, 'enableTinyMCEButton' ), // Callback
		'related_services_settings_general_page',
		'related_services_settings_general_section'
	);

	// auto parse
	add_settings_field(
		'auto_parse_posts',
		__('Replace in posts automatically'),
		array( $this, 'autoParsePosts' ), // Callback
		'related_services_settings_general_page',
		'related_services_settings_general_section'
	);
	add_settings_field(
		'auto_parse_pages',
		__('Replace in pages automatically'),
		array( $this, 'autoParsePages' ), // Callback
		'related_services_settings_general_page',
		'related_services_settings_general_section'
	);
*/