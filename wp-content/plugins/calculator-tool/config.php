<?php

define('CALCULATOR_TOOL_VERSION', '0.1');
define('CALCULATOR_TOOL_MINIMUM_WP_VERSION', '4.2.0');
define('CALCULATOR_TOOL_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CALCULATOR_TOOL_PLUGIN_RELATIVE_DIR', dirname(plugin_basename(__FILE__)));
define('CALCULATOR_TOOL_PLUGIN_DIR', plugin_dir_path(__FILE__));