<?php 
/*
 * Template Name: Homepage Template
 */

get_header(); ?>
<main role="main">
	<h3 class="section-title">Cómo funciona</h3>
	<div class="container">
		<p>Solicitar uno de nuestros minicréditos es muy fácil: lo podrás hacer en casa, sin esperas ni desplazamientos. En esta web tienes toda la información que necesitas para poder hacer uso de nuestros servicios: Política de Privacidad, condiciones, requisitos y todos los aspectos relacionados con nuestra financiación. Y si tienes alguna duda, puedes ponerte en contacto con nosotros por correo electrónico o por teléfono.</p>
		<p>Es imprescindible que nos indiques un DNI/NIE válido, una dirección de e-mail correcta y un número de teléfono móvil operativo, puesto que solicitaremos la confirmación de tus datos por estas vías. Y para la verificación, puedes usar Instantor o enviarnos los comprobantes como archivos adjuntos. <strong>¡Solo tardamos 10 minutos en conceder nuestros minicréditos rápidos!</strong></p>

		<ol class="steps">
			<li>
				<div class="title">Rellena el formulario online</div>
				Para poder disfrutar de nuestros minicréditos online es necesario que te registres en nuestra web. Para ello solo tienes que rellenar un formulario en el que te solicitaremos tus datos personales, un e-mail y un número de teléfono de contacto para poder confirmar la solicitud.
			</li>
			<li>
				<div class="title">Elige la cantidad y el plazo que quieras</div>
				Tú decides las condiciones de nuestros minicréditos: cuánto dinero recibir y en qué fecha devolverlo. Solo tienes que seleccionarlos desplazando las barras de cantidad y plazo. La primera vez que solicites nuestros servicios, puedes elegir entre 50€ y 300€ y entre 5 y 31 días.
			</li>
			<li>
				<div class="title">Espera unos minutos y aprobaremos tu mini crédito al instante</div>
				Tras enviar tus datos y confirmarlos mediante SMS, procesaremos tu solicitud automáticamente gracias a nuestro software informático. No tendrás que esperar mucho, pues nuestros minicréditos rápidos están listos en solo unos minutos. Tras ese periodo, recibirás tu dinero en la cuenta bancaria que nos hayas indicado.
			</li>
		</ol>
	</div>
	<div class="clearfix available-24-7">
		<div class="col-xs-12 col-sm-4 item">
			<div class="line-1">24 Horas</div>
			<div class="line-2">al día</div>
		</div>
		<div class="col-xs-12 col-sm-4 item">
			<div class="line-1">7 días</div>
			<div class="line-2">a la semana</div>
		</div>
		<div class="col-xs-12 col-sm-4 item">
			<div class="line-1">365 días</div>
			<div class="line-2">al año</div>
		</div>
	</div>
	<?php

	$posts = get_posts(array(
		'post_type' => 'servicio',
		'posts_per_page' => 12
	));
	if (!empty($posts)): ?>
		<h3 class="section-title">Servicios destacados</h3>
		<div class="container">
			<div class="services">
				<div class="row compact clearfix">
					<?php foreach ($posts as $post): ?>
						<div class="col-xs-12 col-sm-4 service text-center">
							<div class="inner">
								<h4><a class="title" href="<?php echo get_permalink($post->ID)?>"><?php echo $post->post_title; ?></a></h4>
								<div class="excerpt">
									<?php echo $post->post_excerpt; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="advantages">
		<div class="container">
			<div class="row compact">
				<div class="col-xs-12 col-sm-4 column">
					<h3 class="section-title">Ventajas</h3>
				</div>
				<div class="col-xs-12 col-sm-4 column">
					<div class="item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/time.png" width="32" height="32" alt=""/>
						<div class="title">Somos rápidos</div>
						Te proporcionamos un mini crédito al instante: en apenas 10 minutos estará listo
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 column">
					<div class="item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/gear.png" width="32" height="32" alt=""/>
						<div class="title">Somos flexibles</div>
						En nuestra web admitimos solicitudes que son rechazadas por otros prestamistas
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 column">
					<div class="item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/euro.png" width="32" height="32" alt=""/>
						<div class="title">Garantía de transparencia</div>
						Te mostraremos en todo momento cuáles son los gastos de nuestros minicréditos
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 column">
					<div class="item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/handshake.png" width="32" height="32" alt=""/>
						<div class="title">Respaldo internacional</div>
						Formamos parte de un grupo financiero con gran prestigio y experiencia internacional
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 column">
					<div class="item">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/helpdesk.png" width="32" height="32" alt=""/>
						<div class="title">Estamos a tu disposición</div>
						Nuestro equipo de atención te dará ayuda relacionada con nuestros minicréditos online
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>
