<?php

class RelatedServicesWidget extends WP_Widget {
	protected static $instance;

	public function __construct() {
		$config = array(
			'classname' => 'related-services',
			'description' => __('Servicios relacionados')
		);
		parent::__construct('related_services', __('Servicios relacionados'), $config);
		$this->alt_option_name = 'servicios relacionados';
	}

	public function widget($args, $instance) {
		$options = wp_parse_args(get_option('related_services_general_options'), array());
		?>
		<div class="widget related-services">
			<h3>Minicréditos más solicitados</h3>
			<?php if (!empty($options['services'])): ?>
				<ul>
					<?php foreach ($options['services'] as $service_id):
						if ($service_id == get_the_ID() && !empty($options['alt-service'])) {
							$service_id = $options['alt-service'];
						}
						?>
						<li>
							<a href="<?php echo get_permalink($service_id); ?>"><?php echo get_the_title($service_id) ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
		<?php
	}
}