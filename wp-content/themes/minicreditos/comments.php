<div class="post-comments">
	<div class="row heading">
		<div class="col-xs-5">
			<h2><?php _e('Comments'); ?></h2>
		</div>
		<?php if (comments_open()): ?>
			<div class="col-xs-7 text-right">
				<a href="#respond" class="btn btn-primary"><?php _e('Nuevo comentario', 'holaluz'); ?></a>
			</div>
		<?php endif; ?>
	</div>

	<?php
	$have_comments = have_comments();
	$comments_disabled = !comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments');
	$password_protection = post_password_required();

	if ($password_protection || $have_comments): ?>
		<div class="post-box">
			<div class="comments">
				<?php if ($password_protection): ?>
					<p><?php _e('Este post está protegido por contraseña', 'holaluz'); ?></p>
				<?php elseif ($have_comments) : ?>
					<ul>
						<?php wp_list_comments(array(
							'type' => 'comment',
							'callback' => 'holaluz_comments',
							'style' => 'ul',
						)); ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;

	$req = get_option('require_name_email');
	$required_text = sprintf(' ' . __('Required fields are marked %s'), '<span class="required">*</span>');
	?>
	<?php if ($comments_disabled) : ?>
		<p><?php _e('Se han desactivado los comentarios para esta entrada.', 'holaluz'); ?></p>
	<?php else: ?>
		<div class="reply-heading post-box">
			<h3 id="reply-title" class="comment-reply-title"><?php comment_form_title(__('Leave a Reply'), __('Leave a Reply to %s')) ?></h3>
			<p class="comment-notes">
				<span id="email-notes"><?php _e('Your email address will not be published.') ?></span>
				<?php
				if ($req) {
					echo $required_text;
				}
				?>
			</p>
		</div>

		<?php comment_form(array(
			'class_submit' => 'btn btn-primary btn-submit',
			'comment_notes_before' => '',
			'title_reply' => '',
			'title_reply_to' => '',
			'title_reply_before' => '',
			'title_reply_after' => '',
			'label_submit' => __('Publicar', 'holaluz')
		));
	endif; ?>
</div>
