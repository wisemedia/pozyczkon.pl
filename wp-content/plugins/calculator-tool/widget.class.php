<?php

class CalculatorToolWidget extends WP_Widget {
	protected static $instance;

	public function __construct() {
		$config = array(
			'classname' => 'calculator-tool',
			'description' => __('Calculator Tool')
		);
		parent::__construct('calculator_tool', __('Calculator Tool'), $config);
		$this->alt_option_name = 'calculator tool';
		add_action('wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts'));
		add_action('wp_ajax_my_action', 'my_action_callback');
		function get_customer_levels_callback() {
			echo '33';
			wp_die();
		}
	}

	public function widget($args, $instance) {
		self::calculator();
	}

	public function enqueue_scripts() {
		wp_enqueue_script('bootstrap-slider', CALCULATOR_TOOL_PLUGIN_URL . 'vendor/bootstrap-slider/dist/bootstrap-slider.js', array('jquery'));
		wp_enqueue_style('bootstrap-slider', CALCULATOR_TOOL_PLUGIN_URL . 'vendor/bootstrap-slider/dist/css/bootstrap-slider.css');
		wp_enqueue_script('calculator-tool', CALCULATOR_TOOL_PLUGIN_URL . 'calculator-tool.js', array('jquery'));
		wp_enqueue_style('calculator-tool', CALCULATOR_TOOL_PLUGIN_URL . 'calculator-tool.css');
	}

	public static function calculator() {
		// fetch data
		$response = wp_remote_get(esc_url_raw('https://www.creditocajero.es/products/short_term.json'), array(
			'timeout' => 3
		));
		$api_response = json_decode(wp_remote_retrieve_body($response), true);
		if (!empty($api_response)):
			?>
			<div class="container">
				<div class="calculator-tool"<?php foreach ($api_response as $k => $v): echo "data-{$k}=\"{$v}\""; endforeach; ?>>
					<form class="form" method="post" action="<?php echo get_permalink(get_page_by_path('solicitar'))?>" novalidate>
						<input type="hidden" name="step" value="0">
						<div class="row">
							<div class="col-xs-12 col-sm-7 text-center left-column">
								<div class="range">
									<div class="range-amount">
										<div class="title-4">¿Cuánto dinero quieres?</div>
										<div class="slider-container">
											<span class="loan_min_sum slider-label"><?php echo $api_response['min_sum']; ?> €</span>
											<input id="loan_application_base_amount" type="range" name="amount" data-slider-handle="custom" data-slider-tooltip="hide" data-slider-value="<?php echo $api_response['default_sum']; ?>" value="<?php echo $api_response['default_sum']; ?>" data-slider-min="<?php echo $api_response['min_sum']; ?>" min="<?php echo $api_response['min_sum']; ?>" data-slider-max="<?php echo $api_response['max_sum']; ?>" max="<?php echo $api_response['max_sum']; ?>" data-slider-step="<?php echo $api_response['sum_step']; ?>">
											<span class="loan_max_sum slider-label"><?php echo $api_response['max_sum']; ?> €</span>
										</div>
									</div>
									<div class="range-period">
										<div class="title-4">¿En cuánto tiempo quieres devolverlo?</div>
										<div class="slider-container">
											<span class="loan_min_period slider-label"><?php echo $api_response['min_period']; ?></span>
											<input id="loan_application_period" type="range" name="period" data-slider-handle="custom" data-slider-tooltip="hide" data-slider-value="<?php echo $api_response['default_period']; ?>" value="<?php echo $api_response['default_period']; ?>" data-slider-min="<?php echo $api_response['min_period']; ?>" min="<?php echo $api_response['min_period']; ?>" data-slider-max="<?php echo $api_response['max_period']; ?>" max="<?php echo $api_response['max_period']; ?>" data-slider-step="<?php echo $api_response['period_step']; ?>">
											<span class="loan_max_period slider-label"><?php echo $api_response['max_period']; ?></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-5 right-column">
								<div class="right-inner">
									<div class="loan">
										<div class="inner">
											<div class="row">
												<div class="col-xs-4">
													<label>
														Préstamo
														<div class="value loan_amount"></div>
													</label>
												</div>
												<div class="col-xs-4">
													<label>
														Gastos
														<div class="value loan_price"></div>
													</label>
												</div>
												<div class="col-xs-4">
													<label>
														A devolver
														<div class="value loan_net_amount"></div>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row email">
										<div class="col-xs-12 inner">
											<label for="loan-email">Email</label>
											<input class="form-control" required type="email" name="email" id="loan-email">
											<span class="error-message">No es válido</span>
										</div>
									</div>
									<div class="row call-to-action">
										<div class="col-xs-12 inner">
											<button type="submit" class="btn btn-action">Solicita tu préstamo</button>
											<a class="privacy-link" href="<?php echo get_permalink(get_page_by_path('politica-de-privacidad')) ?>">Pulsando "Solicita", aceptas la Política de Privacidad</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		<?php endif;
	}
}