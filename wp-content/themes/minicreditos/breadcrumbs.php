<?php $url = parse_url(get_home_url()); ?>
<div class="breadcrumbs">
	<ul>
		<li class="home" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="http://www.holaluz.com/" title="<?php _e('Ir a la web', 'holaluz') ?>" itemprop="url"><span itemprop="title">HolaLuz</span></a>
		</li>
		<?php if (!is_home()): ?>
			<li class="blog" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="<?php echo get_home_url(); ?>" title="<?php _e('Ir al blog', 'holaluz') ?>" itemprop="url"><span itemprop="title">Blog</span></a>
			</li>
			<?php if (is_category() || is_single()): ?>
				<li>
					<?php
					if (is_single()):
						//the_category(' , ');
						$category = get_the_category();
						echo $category[0]->cat_name;
						echo '</li><li>';
						the_title();
					else:
						single_cat_title();
					endif;
					?>
				</li>
			<?php elseif (is_page()): ?><?php if ($post->post_parent): ?><?php
				$anc = get_post_ancestors($post->ID);
				$title = get_the_title();
				foreach ($anc as $ancestor) {
					$output = '<li><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a><span>/</span></li>';
					echo $output;
				}
				?>
				<strong title="<?php echo $title; ?>"><?php echo $title ?></strong>
			<?php else: ?>
				<li><?php echo get_the_title() ?></li>
			<?php endif; ?><?php elseif (is_tag()): single_tag_title(); ?><?php elseif (is_day()): ?>
				<li><?php _e('Archive for', 'holaluz'); ?><?php the_date(); ?></li>
			<?php elseif (is_month()): ?>
				<li><?php _e('Archive for', 'holaluz'); ?><?php the_time('F, Y'); ?></li>
			<?php elseif (is_year()): ?>
				<li><?php _e('Archive for', 'holaluz'); ?><?php the_time('Y'); ?></li>
			<?php elseif (is_author()): ?>
				<li><?php _e('Author Archive', 'holaluz') ?></li>
			<?php elseif (isset($_GET['paged']) && !empty($_GET['paged'])): ?>
				<li><?php _e('Blog Archives', 'holaluz') ?></li>
			<?php elseif (is_search()): ?>
				<li><?php _e('Search Results', 'holaluz') ?></li>
			<?php endif; ?><?php else: ?>
			<li class="blog" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<span itemprop="title">Blog</span>
			</li>
		<?php endif; ?>
	</ul>
</div>