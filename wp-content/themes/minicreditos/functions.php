<?php

function theme_enqueue_scripts() {
	//css
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/vendors/bootstrap/css/bootstrap.min.css', array(), '3.3.7');
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/vendors/font-awesome/css/font-awesome.css', array(), '4.6.3');
	wp_enqueue_style('theme', get_template_directory_uri() . '/assets/css/style.css', array(), '0.0.2');
	if (defined('CDP_COOKIES_URL_HTML')) {
		wp_dequeue_style('front-estilos');
	}
	wp_enqueue_style('formulario', get_template_directory_uri() . '/assets/css/formulario.css', array(), '0.0.1');

	//js
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/vendors/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.7');
	wp_enqueue_script('theme', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '0.0.2');
	if (is_singular()) {
		wp_enqueue_script('comment-reply');
	}
}

add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');

function remove_jquery_migrate(&$scripts) {
	if (!is_admin()) {
		$scripts->remove('jquery');
		$scripts->add('jquery', false, array('jquery-core'));
	}
}

add_filter('wp_default_scripts', 'remove_jquery_migrate');

//languages
load_theme_textdomain('minicreditos', get_template_directory() . '/languages');

//Mides thumbs
add_theme_support('post-thumbnails');
add_theme_support('html5');

register_nav_menus(array(
	'header' => __('Menú header', 'minicreditos'),
	'footer' => __('Menú footer', 'minicreditos'),
));

//Sidebars
add_action('widgets_init', 'minicreditos_register_sidebar');
function minicreditos_register_sidebar() {
	register_sidebar(array(
		'name' => 'Default Sidebar',
		'id' => 'minicreditos-sidebar',
		'description' => __('Sidebar principal', 'minicreditos'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	));
}

function theme_seo_canonical($canonical) {
	if (is_tag()) {
		return false;
	}
}

add_filter('wpseo_canonical', 'theme_seo_canonical', 2, 10);

add_action('init', 'service_post_type');
function service_post_type() {
	$slug = 'servicio';
	register_post_type($slug,
		array(
			'labels' => array(
				'name' => __('Servicios'),
				'singular_name' => __('Servicio')
			),
			'public' => true,
			'menu_position' => 5,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt',
			),
			'has_archive' => true,
			'rewrite' => array('slug' => $slug),
		)
	);
	add_theme_support('post-thumbnails', array('post', $slug));
}


function remove_service_slug($post_link, $post, $leavename) {

	if ('servicio' != $post->post_type || 'publish' != $post->post_status) {
		return $post_link;
	}

	$post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);

	return $post_link;
}

add_filter('post_type_link', 'remove_service_slug', 10, 3);

function find_service_without_slug($query) {
	// Only noop the main query
	if (!$query->is_main_query()) {
		return;
	}

	// Only noop our very specific rewrite rule match
	if (2 != count($query->query) || !isset($query->query['page'])) {
		return;
	}

	// 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
	if (!empty($query->query['name'])) {
		$query->set('post_type', array('post', 'page', 'servicio'));
	}
}

add_action('pre_get_posts', 'find_service_without_slug');

/* Change Excerpt length */
function custom_excerpt_length( $length ) {
return 15;
}
add_filter( "excerpt_length", "custom_excerpt_length", 999 );

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


add_image_size( 'latest-post', 210, 140, array('center', 'center'));
add_image_size( 'post-list', 360, 360, array('center', 'center'));

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);