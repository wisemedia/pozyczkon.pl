<?php get_header(); ?>
<?php if (have_posts()): the_post(); ?>
	<div class="container">
		<div class="row">
			<main role="main" class="col-xs-12 col-md-8">
				<div class="post" itemscope itemtype="http://schema.org/BlogPosting">
					<div class="post-content" itemprop="articleBody">
						<?php the_content(); ?>
					</div>
				</div>
			</main>
			<div class="hidden-xs hidden-sm col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php get_footer(); ?>
